class Employee(object):
    def __init__(self, f_name, l_name, pay, title):
        self.f_name = f_name
        self.l_name = l_name
        self.pay = pay
        self.title = title
        self.email = f_name  + '.' + l_name + '.' + '@company.com'

    def __str__(self):
        return self.f_name + self.l_name
