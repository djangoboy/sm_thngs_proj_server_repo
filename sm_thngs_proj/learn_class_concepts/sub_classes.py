from learn_class_concepts.parent_classes import Employee
#from (class complete path) import class <--Structure

class Developer(Employee): # --> but what if when we want to add some extra variables to this class <--
    def __init__(self, f_name, l_name, pay, title, prog_lang): #<--shows all the fields that are going to be constructed in deveoper class
        super().__init__(f_name, l_name, pay, title)            #<--as we have already handled most of them in parent class so  so use super to mention what needs to be handled by parent class
        self.prog_lang = prog_lang

    def __str__(self):                      #<--THIS HOW WE OVERRIDE METHODS FROM PARENT CLASS
        return self.f_name + ', Email: ' + self.email


d1 = Developer(f_name='Deepak',l_name='Singh', pay= 25000, title='Developer', prog_lang='Python')
print(d1)
print(d1.f_name)
print(d1.email)
print(d1.prog_lang)


#print(Employee.f_name(d1)) <-- why is it not printing
