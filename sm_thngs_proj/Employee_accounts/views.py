from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Employee
from .forms import EmployeeForm
from django.shortcuts import render, HttpResponse


class EmployeeListView(ListView):
    model = Employee
    template_name = 'Employee_accounts/employee_list_func.html'
    context_object_name = 'all_emp'

    #how to override querying database
    # def get_queryset(self):
    #     all_emp = Employee.objects.filter(f_name='test_fname')

def employeelistview(request):
    all_emp = Employee.objects.all()
    return render(request, template_name='Employee_accounts/employee_list.html', context={'object_list':all_emp})


class EmployeeCreateView(CreateView):
    model = Employee
    form_class = EmployeeForm

def employeecreateview(request):
    if request.mehtod =='GET':
        form  = EmployeeForm()
    if request.mehtod =='POST':
        pass

class EmployeeDetailView(DetailView):
    model = Employee


class EmployeeUpdateView(UpdateView):
    model = Employee
    form_class = EmployeeForm

#all forms
def greetform(request):
    if request.method == 'GET':
        print(request)
        return render(request, template_name='test_forms.html')

    else:
        # print(dir(POST.DATA)) ## HOW TO CAPTURE DATA THAT'S COMMING FROM FORM USING POST METHOD
        return render(request, template_name='test_forms.html', context={'object_list': 'i am here as context value'})

