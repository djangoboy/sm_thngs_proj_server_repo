# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-13 12:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Employee_accounts', '0003_author'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Author',
        ),
    ]
