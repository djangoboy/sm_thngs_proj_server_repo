# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-13 12:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Employee_accounts', '0002_auto_20180211_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('active', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField()),
                ('last_logged_in', models.DateTimeField()),
            ],
            options={
                'ordering': ('-pk',),
            },
        ),
    ]
