from django.conf.urls import url
from . import views



urlpatterns = (
    # urls for Employee
    url(r'^employee/$', views.EmployeeListView.as_view(), name='Employee_accounts_employee_list'),
   )

#which means we can append more urls that can be specific to any apps
urlpatterns += (
    # urls for Employee
    url(r'^list/$', views.EmployeeListView.as_view(), name='Employee_accounts_employee_list'),
    #url(r'^employee/list/$', views.employeelistview, name='employeelistview'),

    url(r'^create/$', views.EmployeeCreateView.as_view(), name='Employee_accounts_employee_create'),
    #url(r'^employee/create/$', views.employeecreateview, name='employeecreateview'),

    url(r'^detail/(?P<slug>\S+)/$', views.EmployeeDetailView.as_view(), name='Employee_accounts_employee_detail'),
    url(r'^update/(?P<slug>\S+)/$', views.EmployeeUpdateView.as_view(), name='Employee_accounts_employee_update'),
)


#ALL FORMS URL
urlpatterns += (

    url(r'^greetform/$', views.greetform, name='greetform'),

)
