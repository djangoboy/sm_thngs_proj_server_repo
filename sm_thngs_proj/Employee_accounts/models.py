from django.core.urlresolvers import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields


class Employee(models.Model):

    # Fields
    f_name = models.CharField(max_length=50)
    l_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20, null=False)
    website = models.URLField(max_length=50, null=True)
    address = models.CharField(max_length=255, null=False)
    about = models.TextField(max_length=500, null=False)
    slug = extension_fields.AutoSlugField(unique=True, populate_from='f_name')
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('accounts:Employee_accounts_employee_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('accounts:Employee_accounts_employee_update', args=(self.slug,))



#creating subclass(Developer) that inherits from parent class(Employee)
# class Developer(Employee):
# we can have normal class topics in this as this is models.py and only database models are suppose to be here.
#we are going to create a seprate package to test classes and methods in them and how to inherit from a class and override any of it's behaviour