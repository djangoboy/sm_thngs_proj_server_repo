from django.contrib import admin
from django import forms
from .models import Employee

class EmployeeAdminForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = '__all__'


class EmployeeAdmin(admin.ModelAdmin):
    form = EmployeeAdminForm
    list_display = ['f_name', 'l_name', 'slug', 'created', 'last_updated']
    #readonly_fields = ['name', 'slug', 'created', 'last_updated']

admin.site.register(Employee, EmployeeAdmin)


