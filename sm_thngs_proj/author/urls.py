from django.conf.urls import url
from . import views



urlpatterns = (
    # urls for Author
    url(r'^list/$', views.authorListView, name='authorListView'),

    url(r'^create/$', views.authorCreateView, name='authorCreateView'),

    url(r'^detail/(?P<pk>[0-9])/$', views.authorDetailView, name='authorDetailView'),

    url(r'^update/(?P<slug>\S+)/$', views.authorUpdateView, name='authorUpdateView'),
)