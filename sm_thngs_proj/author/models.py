from django.db import models
from django.core.urlresolvers import reverse


# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=100, unique=True)
    email = models.EmailField(unique=True)
    active = models.BooleanField(default=False)
    created_on = models.DateTimeField()
    last_logged_in = models.DateTimeField()


    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('accounts:author_accounts_employee_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('accounts:Employee_accounts_employee_update', args=(self.slug,))
