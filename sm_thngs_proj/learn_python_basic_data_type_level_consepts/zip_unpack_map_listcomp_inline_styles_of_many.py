list_fname = ['deepak', 'mak', 'munna']
list_lname = ['singh', 'Shah', 'singh']

#mapping

#suppose you we need a list of many full names from both the lists

#Bad way -- if we need three full names indivisually
# list_fname[0]+list_lname[0]
# list_fname[0]+list_lname[1]
# list_fname[0]+list_lname[2]
#we need it in this form <-- list_fname[0]+list_lname[0]

# throws error
# for i, j in list_fname, list_lname:
#     print(i, j)

#Good way

# full_name = [*list_fname]


